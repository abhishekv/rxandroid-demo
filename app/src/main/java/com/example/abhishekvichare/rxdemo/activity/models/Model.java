package com.example.abhishekvichare.rxdemo.activity.models;

/**
 * Created by Abhishekvichare on 03/02/18.
 */

public class Model {

    private String firstName;

    private String lastName;

    private String currentTime;

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Model{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", currentTime='" + currentTime + '\'' +
                '}';
    }
}
