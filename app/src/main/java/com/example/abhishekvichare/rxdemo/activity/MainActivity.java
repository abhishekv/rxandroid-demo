package com.example.abhishekvichare.rxdemo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.abhishekvichare.rxdemo.R;
import com.example.abhishekvichare.rxdemo.activity.models.Model;
import com.example.abhishekvichare.rxdemo.activity.observables.CustomObservable;
import com.example.abhishekvichare.rxdemo.activity.observers.CustomObserver;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.button)
    Button button;

    @BindView(R.id.button2)
    Button stopBtn;

  //  private Observable<Model> customObservable = Observable.create(new CustomObservable());
    private CustomObserver customObserver = new CustomObserver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        /*customObservable.observeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());*/

        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customObserver.stopObserver();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customObserver.startObserver();
                /*customObservable.subscribe(customObserver);*/
            }
        });
    }
}
