package com.example.abhishekvichare.rxdemo.activity.observables;

import com.example.abhishekvichare.rxdemo.activity.models.Model;

import io.reactivex.Observable;

/**
 * Created by Abhishekvichare on 03/02/18.
 */

public class ObservableFactory {

    public static Observable getObservable(String title){
        return Observable.create(new CustomObservable());
    }
}
