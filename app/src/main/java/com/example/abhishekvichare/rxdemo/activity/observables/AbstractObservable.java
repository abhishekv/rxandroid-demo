package com.example.abhishekvichare.rxdemo.activity.observables;

import com.example.abhishekvichare.rxdemo.activity.models.Model;

import io.reactivex.ObservableOnSubscribe;

/**
 * Created by Abhishekvichare on 03/02/18.
 */

public abstract class AbstractObservable<T> implements ObservableOnSubscribe<T> {


    public abstract void stop();
}
