package com.example.abhishekvichare.rxdemo.activity.observers;

import android.util.Log;

import com.example.abhishekvichare.rxdemo.activity.models.Model;
import com.example.abhishekvichare.rxdemo.activity.observables.CustomObservable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Abhishekvichare on 03/02/18.
 */

public class CustomObserver extends AbstractObserver<Model> {

    public final String TAG = this.getClass().getSimpleName();

    private Observable<Model> customObservable ;//= Observable.create(new CustomObservable());
    private CustomObservable customObservableInst;

    public CustomObserver() {
        customObservableInst=new CustomObservable();
        customObservable = Observable.create(customObservableInst);
        customObservable.observeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void onSubscribe(Disposable d) {
        Log.d(TAG, "onSubscribe --> " + d.isDisposed());

    }

    @Override
    public void onNext(Model model) {
        Log.d(TAG, "Model found " + model.toString());
    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onComplete() {

    }


    @Override
    public void startObserver() {

        customObservable.subscribe(this);
    }

    @Override
    public void stopObserver() {
        customObservableInst.stop();
    }
}
