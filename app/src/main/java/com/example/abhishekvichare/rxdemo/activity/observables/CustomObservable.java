package com.example.abhishekvichare.rxdemo.activity.observables;

import android.os.CountDownTimer;
import android.util.Log;

import com.example.abhishekvichare.rxdemo.activity.models.Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import io.reactivex.ObservableEmitter;

/**
 * Created by Abhishekvichare on 03/02/18.
 */

public class CustomObservable extends AbstractObservable {

    private List<ObservableEmitter<Model>> emitterList;

    private final String TAG=this.getClass().getSimpleName();

    public CustomObservable() {

        this.emitterList = new ArrayList<>();
    }

    @Override
    public void subscribe(final ObservableEmitter emitter) throws Exception {

        final Model model = new Model();
        model.setFirstName("Abhishek");
        model.setLastName("Vichare");


        new CountDownTimer(30000, 1000) {

            @Override
            public void onTick(long l) {
                model.setCurrentTime(new Date().toString());
                emitter.onNext(model);
            }

            @Override
            public void onFinish() {
                emitter.onComplete();
            }
        }.start();

        emitterList.add(emitter);
        Log.d(TAG,"New Subscriber added");
    }


    @Override
    public void stop() {
        ListIterator<ObservableEmitter<Model>> itr = emitterList.listIterator();
        while (itr.hasNext()) {
            ObservableEmitter<Model> emitter = itr.next();
            if (!emitter.isDisposed()) {
                emitter.onComplete();
                itr.remove();
                Log.d(TAG,"Subscriber removed");
            }
        }
    }

}
