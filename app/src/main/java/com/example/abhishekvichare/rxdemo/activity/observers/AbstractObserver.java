package com.example.abhishekvichare.rxdemo.activity.observers;

import io.reactivex.Observer;

/**
 * Created by Abhishekvichare on 04/02/18.
 */

public abstract class AbstractObserver<T> implements Observer<T> {

    public abstract void startObserver();

    public abstract void stopObserver();
}
